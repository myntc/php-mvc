<?php

class Product extends Controller
{
    function index()
    {
        $productModel = $this->getModel('ProductModel');
        $products = $productModel->getAll();

        $this->getView('1column',
            [
                'page' => 'product/index',
                'products' => $products,
            ]);
    }
}
