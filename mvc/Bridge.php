<?php

// Process URL from browser
require_once './mvc/core/App.php';

// How controllers call Views & Models
require_once './mvc/core/Controller.php';

// Database
require_once './mvc/core/Database.php';

// Model
require_once './mvc/core/Model.php';

// Url Builder
require_once './mvc/core/UrlBuilder.php';
