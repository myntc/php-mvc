<?php

interface UrlBuilderInterface
{
    public static function getBaseUrl();
    public static function getUrl($uri);
}
