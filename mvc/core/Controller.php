<?php

class Controller
{
    public function getModel($model)
    {
        if (file_exists("./mvc/models/$model.php")) {
            require_once "./mvc/models/$model.php";
            return new $model;
        }
        throw new Exception("Not found $model class.");
    }

    public function getView($view, $data = [])
    {
        require_once "./mvc/views/$view.php";
    }
}
