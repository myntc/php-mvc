<?php

class Database
{
    const HOST = 'localhost';
    const USER = 'root';
    const PASSWORD = '123456';
    const DATABASE = 'game';

    protected $connection;

    public function __construct()
    {
        $this->connection = mysqli_connect(self::HOST, self::USER, self::PASSWORD, self::DATABASE) or die ('Error Connection.');
        mysqli_set_charset($this->connection, 'utf8');
    }

    public function insert($table, array $data)
    {
        $sql = "INSERT INTO {$table} ";
        $columns = implode(',', array_keys($data));
        $values = '';
        $sql .= '(' . $columns . ')';

        foreach ($data as $field => $value) {
            if (is_string($value)) {
                $values .= '\'' . mysqli_real_escape_string($this->connection, $value) . '\',';
            } else {
                $values .= mysqli_real_escape_string($this->connection, $value) . ',';
            }
        }

        $values = substr($values, 0, -1);
        $sql .= ' VALUES (' . $values . ')';
        // _debug($sql);di
        mysqli_query($this->connection, $sql) or die('[Insert] The error query: ' . mysqli_error($this->connection));

        return mysqli_insert_id($this->connection);
    }

    public function update($table, array $data, array $conditions)
    {
        $sql = "UPDATE {$table}";
        $set = ' SET ';
        $where = ' WHERE ';

        foreach ($data as $field => $value) {
            if (is_string($value)) {
                $set .= $field . '=' . '\'' . mysqli_real_escape_string($this->connection, $value) . '\',';
            } else {
                $set .= $field . '=' . mysqli_real_escape_string($this->connection, $value) . ',';
            }
        }

        $set = substr($set, 0, -1);


        foreach ($conditions as $field => $value) {
            if (is_string($value)) {
                $where .= $field . '=' . '\'' . mysqli_real_escape_string($this->connection, $value) . '\' AND ';
            } else {
                $where .= $field . '=' . mysqli_real_escape_string($this->connection, $value) . ' AND ';
            }
        }

        $where = substr($where, 0, -5);
        $sql .= $set . $where;
        // _debug($sql);die;
        mysqli_query($this->connection, $sql) or die('[Update] The error query: ' . mysqli_error($this->connection));

        return mysqli_affected_rows($this->connection);
    }

    public function countTable($table)
    {
        $sql = "SELECT id FROM  {$table}";
        $result = mysqli_query($this->connection, $sql) or die('[Count] The error query: ' . mysqli_error($this->connection));
        return mysqli_num_rows($result);
    }

    public function delete($table, $id)
    {
        $sql = "DELETE FROM {$table} WHERE id = $id ";

        mysqli_query($this->connection, $sql) or die ('[Delete] The error query: ' . mysqli_error($this->connection));
        return mysqli_affected_rows($this->connection);
    }

    public function fetchSql($sql)
    {
        $result = mysqli_query($this->connection, $sql) or die('[FetchSql] The error query: ' . mysqli_error($this->connection));
        $data = [];
        if ($result) {
            while ($num = mysqli_fetch_assoc($result)) {
                $data[] = $num;
            }
        }
        return $data;
    }

    public function fetchById($table, $id)
    {
        $sql = "SELECT * FROM {$table} WHERE id = $id ";
        $result = mysqli_query($this->connection, $sql) or die('[FetchById] The error query: ' . mysqli_error($this->connection));
        return mysqli_fetch_assoc($result);
    }

    public function fetchOne($table, $query)
    {
        $sql = "SELECT * FROM {$table} WHERE ";
        $sql .= $query;
        $sql .= "LIMIT 1";
        $result = mysqli_query($this->connection, $sql) or die('[FetchOne] The error query: ' . mysqli_error($this->connection));
        return mysqli_fetch_assoc($result);
    }

    public function fetchAll($table)
    {
        $sql = "SELECT * FROM {$table} WHERE 1";
        $result = mysqli_query($this->connection, $sql) or die('[FetchAll] The error query: ' . mysqli_error($this->connection));
        $data = [];
        if ($result) {
            while ($num = mysqli_fetch_assoc($result)) {
                $data[] = $num;
            }
        }
        return $data;
    }

    public function fetchJones($table, $sql, $total = 1, $page, $row, $pagination = false)
    {
        $data = [];

        if ($pagination == true) {
            $total = $this->countTable($table);
            $pageQty = ceil($total / $row);
            $start = ($page - 1) * $row;
            $sql .= " LIMIT $start,$row ";
            $data = ["page" => $pageQty];
            $result = mysqli_query($this->connection, $sql) or die('[FetchJones] The error query: ' . mysqli_error($this->connection));
        } else {
            $result = mysqli_query($this->connection, $sql) or die('[FetchJones] The error query: ' . mysqli_error($this->connection));
        }

        if ($result) {
            while ($num = mysqli_fetch_assoc($result)) {
                $data[] = $num;
            }
        }

        return $data;
    }

    public function total($sql)
    {
        $result = mysqli_query($this->connection, $sql);
        return mysqli_fetch_assoc($result);
    }
}
