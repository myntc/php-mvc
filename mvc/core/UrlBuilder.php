<?php
require_once './mvc/core/interfaces/UrlBuilderInterface.php';
class UrlBuilder implements UrlBuilderInterface
{
    public static function getBaseUrl()
    {
        return sprintf(
            "%s://%s/",
            isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
            $_SERVER['SERVER_NAME']
        );
    }

    public static function getUrl($uri)
    {
        return self::getBaseUrl() . $uri;
    }
}
