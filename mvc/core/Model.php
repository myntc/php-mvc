<?php

class Model
{
    protected $table;
    protected $database;

    public function __construct()
    {
        $this->database = new Database;
    }

    public function getAll()
    {
        return $this->database->fetchAll($this->table);
    }

    public function getById($id)
    {
        return $this->database->fetchById($this->table, $id);
    }
}
