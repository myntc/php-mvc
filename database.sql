-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 13, 2020 at 11:45 PM
-- Server version: 10.1.44-MariaDB-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `game`
--

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `product_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` float DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `quantity` int(11) DEFAULT NULL,
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `name`, `price`, `image`, `description`, `quantity`, `create_at`, `update_at`) VALUES
(0, 'Survival Heroes', 9, 'survival_heroes.jpg', 'Survival Heroes is the best mobile game when combining two of the most popular game genres today, MOBA and Battle Royale.', 1, '2020-09-13 15:50:43', '2020-09-13 15:50:43'),
(1, 'Area F2', 20, 'area_f2.jpg', 'the most attractive action game on the mobile platform today', 5, '2020-09-12 13:45:40', '2020-09-12 13:45:40'),
(2, 'Auto Chess Mobile', 10, 'auto_chess_mobile.jpg', 'Auto Chess Mobile is a version of the PC game called Dota Auto Chess', 2, '2020-09-12 13:48:05', '2020-09-12 13:48:05'),
(3, 'War Robots', 5, 'war_robots.jpg', 'War Robots can be said to be an action game for gamers who like to challenge.', 3, '2020-09-13 15:48:34', '2020-09-13 15:48:34'),
(4, 'Cyber Hunter', 4, 'cyber_hunter.jpg', 'Cyber Hunter builds an extremely thrilling combat universe. The fighting ability of each player is brought up to an extremely superior level\r\n', 8, '2020-09-13 15:54:27', '2020-09-13 15:54:27'),
(5, 'Warface Mobile', 6, 'warface_mobile.jpg', 'Warface Mobile was born from the success of the PC version. Warface Mobile is an action role-playing game that attracts any gamer with intense modes', 9, '2020-09-13 15:52:41', '2020-09-13 15:52:41');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`product_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
